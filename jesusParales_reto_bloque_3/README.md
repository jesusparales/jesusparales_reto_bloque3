Colores
=======

Fuente titulo cards: #FFF
Fuente oscura: #1A1A1A
Fuente naranja: #FF9900
Fuente footer: #5F5F5F
Background Footer: #202020
Background submenu: #202020
Separador submenu: #FFF

Tipos de fuente
===============

Sidebar
-------

Logo: Arial Narrow, Regular, 60px
Subtitulo sidebar: Arial Narrow, Regular, 14px
Menu y submenu sidebar: Arial Narrow, Bold, 14px

Gallery
-------

Fecha card: Arial Rounded MT B, Regular, 12px
Titulo card: Arial Black, Regular, 20px


